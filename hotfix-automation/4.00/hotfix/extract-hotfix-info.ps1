$Current_Directory 	= 	Split-Path -Parent $MyInvocation.MyCommand.Path
	write-host  CURRENTDIR: $Current_Directory
# 	-----------------------------------------------------------------	
# 	Call Environment setup file for environment (dir path / variable)
#	-----------------------------------------------------------------
#	. ("$Current_Directory\hotfix-variables.ps1")

	$THF_Number 				= 	0
	$THF_Target_Update 			= 	0

	$THF_Required_New_Binaries 	= 	0
	$THF_Required_Old_Binaries	= 	0
	
	$Is_Old_Bin					= 	0
	$Is_Old_Jar					= 	0
	$Is_Old_Webapps_War			= 	0
	$Is_Old_Deploy_War			= 	0


foreach($line in [System.IO.File]::ReadLines(("$Current_Directory\thf-requested-artifacts.txt")))
{
       $line
	   
	if($line.equals('[THF-REQUIRED-ARTIFACTS]'))
	{
		$THF_Required_New_Binaries 	= 	1
		$THF_Number 				= 	0
		$THF_Target_Update 			= 	0
		$THF_Required_Old_Binaries	= 	0
		$Is_Old_Bin					= 	0
		$Is_Old_Jar					= 	0
		$Is_Old_Webapps_War			= 	0
		$Is_Old_Deploy_War			= 	0
	}
	elseif($line.equals('[THF-NO]'))
	{
		$THF_Required_New_Binaries 	= 	0
		$THF_Number 				= 	1
		$THF_Target_Update 			= 	0
		$THF_Required_Old_Binaries	= 	0
		$Is_Old_Bin					= 	0
		$Is_Old_Jar					= 	0
		$Is_Old_Webapps_War			= 	0
		$Is_Old_Deploy_War			= 	0
	}		
	elseif( $line.equals('[THF-TARGET-UPDATE]'))
	{		
		$THF_Required_New_Binaries 	= 	0
		$THF_Number 				= 	0
		$THF_Target_Update 			= 	1
		$THF_Required_Old_Binaries	= 	0
		$Is_Old_Bin					= 	0
		$Is_Old_Jar					= 	0
		$Is_Old_Webapps_War			= 	0
		$Is_Old_Deploy_War			= 	0			
	}
	elseif( $line.equals('[THF-OLD-ARTIFACTS-NAMES]'))
	{		
		$THF_Required_New_Binaries 	= 	0
		$THF_Number 				= 	0
		$THF_Target_Update 			= 	0
		$THF_Required_Old_Binaries	= 	1
		$Is_Old_Bin					= 	0
		$Is_Old_Jar					= 	0
		$Is_Old_Webapps_War			= 	0
		$Is_Old_Deploy_War			= 	0				
	}			
	elseif( $line.equals('[THF-OLD-BIN-ARTIFACTS-PATH]'))
	{
		$THF_Required_New_Binaries 	= 	0
		$THF_Number 				= 	0
		$THF_Target_Update 			= 	0
		$THF_Required_Old_Binaries	= 	0
		$Is_Old_Bin					= 	1
		$Is_Old_Jar					= 	0
		$Is_Old_Webapps_War			= 	0
		$Is_Old_Deploy_War			= 	0
	}	
	elseif( $line.equals('[THF-OLD-JAR-ARTIFACTS-PATH]'))
	{		
		$THF_Required_New_Binaries 	= 	0
		$THF_Number 				= 	0
		$THF_Target_Update 			= 	0
		$THF_Required_Old_Binaries	= 	0
		$Is_Old_Bin					= 	0
		$Is_Old_Jar					= 	1
		$Is_Old_Webapps_War			= 	0
		$Is_Old_Deploy_War			= 	0				
	}	
	elseif( $line.equals('[THF-OLD-WEBAPPS-WAR-ARTIFACTS-PATH]'))
	{		
		$THF_Required_New_Binaries 	= 	0
		$THF_Number 				= 	0
		$THF_Target_Update 			= 	0
		$THF_Required_Old_Binaries	= 	0
		$Is_Old_Bin					= 	0
		$Is_Old_Jar					= 	0
		$Is_Old_Webapps_War			= 	1
		$Is_Old_Deploy_War			= 	0
	}
	elseif( $line.equals('[THF-OLD-DEPLOY-WAR-ARTIFACTS-PATH]'))
	{		
		$THF_Required_New_Binaries 	= 	0
		$THF_Number 				= 	0
		$THF_Target_Update 			= 	0
		$THF_Required_Old_Binaries	= 	0
		$Is_Old_Bin					= 	0
		$Is_Old_Jar					= 	0
		$Is_Old_Webapps_War			= 	0
		$Is_Old_Deploy_War			= 	1
	}
	if (-not ([string]::IsNullOrEmpty($line)))
	{
		if( $THF_Target_Update ) 
		{
			$THE_TARGETUPDATE	= 	$line 
		}			
		if( $THF_Number )
		{
			$THFVALUE 			= 	$line	
		}
		if( $Is_Old_Bin )
		{
			$OLDBINPATH 		= 	$line	
		}
		if( $Is_Old_Jar )
		{
			$OLDJARSPATH 		= 	$line	
		}
		if( $Is_Old_Webapps_War )
		{
			$OLDWARSPATH 		= 	$line
		}
		if( $Is_Old_Deploy_War )
		{
			$OLDDeployWARSPATH 	= 	$line
		}
	}
}